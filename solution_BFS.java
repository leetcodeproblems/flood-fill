//BFS Solution
class Solution {
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        
        Queue<int[]> q =new LinkedList<>();
        Queue<int[]> p =new LinkedList<>();
        boolean[][] seen =new boolean[image.length][image[0].length];
        
        q.add(new int[]{sr, sc});
        seen[sr][sc]=true;
        
        int orgColor=image[sr][sc];
        image[sr][sc]=newColor;
        
        
        
        while(!q.isEmpty()){
            //front
            int[] front= q.remove();
            int row= front[0], col = front[1];
            
            //Neighbors
            int[] dx={0, 0, 1, -1};
            int[] dy={1 ,-1, 0, 0};
            for(int i=0; i<4; i++){
                int toRow = row + dx[i];
                int toCol = col + dy[i];
                
                if(check(toRow, toCol, image, seen, orgColor)){
                    p.add(new int[]{toRow, toCol});
                    seen[toRow][toCol]=true;
                    image[toRow][toCol]=newColor;
                }
            }
            
            //level over
            if(q.isEmpty()){
                //swap p and q
                q=p;
                p=new LinkedList<>();
            }
        }
        
        return image;
        
    }
    
    boolean check(int row, int col, int[][] image, boolean[][] seen, int orgColor){
        return (row>=0) && (row < image.length) && (col >= 0) && (col < image[0].length) && (image[row][col] == orgColor) && (!seen[row][col]);
    }

}
